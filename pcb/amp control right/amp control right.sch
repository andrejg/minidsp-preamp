EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L amp-control-right-rescue:Arduino_Nano_v3.x-MCU_Module A1
U 1 1 5CD7CB24
P 6450 3150
F 0 "A1" V 6404 2110 50  0000 R CNN
F 1 "Arduino-Right" V 6495 2110 50  0000 R CNN
F 2 "Module:Arduino_Nano" H 6600 2200 50  0001 L CNN
F 3 "http://www.mouser.com/pdfdocs/Gravitech_Arduino_Nano3_0.pdf" H 6450 2150 50  0001 C CNN
	1    6450 3150
	0    1    1    0   
$EndComp
$Comp
L Device:R R1
U 1 1 5CD7DA4F
P 7750 2150
F 0 "R1" H 7820 2196 50  0000 L CNN
F 1 "750 Ohm +/- 10%" H 7820 2105 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Vertical" V 7680 2150 50  0001 C CNN
F 3 "~" H 7750 2150 50  0001 C CNN
	1    7750 2150
	1    0    0    -1  
$EndComp
NoConn ~ 7450 3250
NoConn ~ 6550 2650
NoConn ~ 6450 2650
NoConn ~ 6350 2650
NoConn ~ 5750 3650
NoConn ~ 6150 2650
NoConn ~ 5950 3650
NoConn ~ 5850 2650
NoConn ~ 5750 2650
NoConn ~ 6650 3650
NoConn ~ 6950 3650
Text GLabel 6000 1400 3    50   Input ~ 0
VIN
Text GLabel 6100 1400 3    50   Input ~ 0
GND
Text GLabel 6900 1400 3    50   Input ~ 0
GND
Text GLabel 6500 1400 3    50   Input ~ 0
GND
Text GLabel 6800 1400 3    50   Input ~ 0
GND
Text GLabel 6300 1400 3    50   Input ~ 0
GND
Text GLabel 6200 1400 3    50   Input ~ 0
PowerSwitch
Text GLabel 7750 2300 3    50   Input ~ 0
PowerLed
Text GLabel 6700 1400 3    50   Input ~ 0
5V
Text GLabel 6600 1400 3    50   Input ~ 0
Relay
Text GLabel 7100 1400 3    50   Input ~ 0
RX
Text GLabel 7000 1400 3    50   Input ~ 0
TX
Text GLabel 7450 3050 2    50   Input ~ 0
VIN
Text GLabel 5450 3250 0    50   Input ~ 0
GND
Text GLabel 6950 2650 1    50   Input ~ 0
TX
Text GLabel 7050 2650 1    50   Input ~ 0
RX
Text GLabel 6250 2650 1    50   Input ~ 0
PowerSwitch
Text GLabel 5950 2650 1    50   Input ~ 0
PowerLed
Text GLabel 7450 3350 2    50   Input ~ 0
5V
Text GLabel 6050 2650 1    50   Input ~ 0
Relay
Text GLabel 7750 2000 1    50   Input ~ 0
PowerLedR
Text GLabel 6400 1400 3    50   Input ~ 0
PowerLedR
NoConn ~ 6750 2650
NoConn ~ 6850 2650
NoConn ~ 6650 2650
NoConn ~ 5450 3150
NoConn ~ 6050 3650
NoConn ~ 6150 3650
NoConn ~ 6250 3650
NoConn ~ 6350 3650
NoConn ~ 6450 3650
$Comp
L Connector:Conn_01x12_Male J1
U 1 1 5CD86407
P 6600 1200
F 0 "J1" V 6435 1127 50  0000 C CNN
F 1 "CTRL-Right" V 6526 1127 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x12_P2.54mm_Vertical" H 6600 1200 50  0001 C CNN
F 3 "~" H 6600 1200 50  0001 C CNN
	1    6600 1200
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x03_Male J2
U 1 1 5CE8091A
P 5450 1200
F 0 "J2" V 5510 1340 50  0000 L CNN
F 1 "LDR-Right" V 5601 1340 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 5450 1200 50  0001 C CNN
F 3 "~" H 5450 1200 50  0001 C CNN
	1    5450 1200
	0    1    1    0   
$EndComp
Text GLabel 5350 1400 3    50   Input ~ 0
LDR_S
Text GLabel 5850 3650 3    50   Input ~ 0
LDR_S
Text GLabel 5550 1400 3    50   Input ~ 0
GND
Text GLabel 5450 1400 3    50   Input ~ 0
5V
$Comp
L Switch:SW_DIP_x01 SW1
U 1 1 5CE82062
P 6300 4700
F 0 "SW1" H 6300 4967 50  0000 C CNN
F 1 "SW_DIP_x01" H 6300 4876 50  0000 C CNN
F 2 "Button_Switch_THT:SW_DIP_SPSTx01_Slide_6.7x4.1mm_W7.62mm_P2.54mm_LowProfile" H 6300 4700 50  0001 C CNN
F 3 "" H 6300 4700 50  0001 C CNN
	1    6300 4700
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C1
U 1 1 5CE82101
P 5850 4700
F 0 "C1" V 5595 4700 50  0000 C CNN
F 1 "10 uF" V 5686 4700 50  0000 C CNN
F 2 "Capacitor_THT:CP_Radial_D4.0mm_P1.50mm" H 5888 4550 50  0001 C CNN
F 3 "~" H 5850 4700 50  0001 C CNN
	1    5850 4700
	0    1    1    0   
$EndComp
Text GLabel 5700 4700 0    50   Input ~ 0
GND
Text GLabel 7050 3650 3    50   Input ~ 0
RST
Text GLabel 6600 4700 2    50   Input ~ 0
RST
$EndSCHEMATC
