EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L minidsp-arduino-hat-rescue:Arduino_Mega2560_Shield-arduino ArduinoMega2560
U 1 1 5CF81881
P 7550 5550
F 0 "ArduinoMega2560" H 7550 3170 60  0000 C CNN
F 1 "Arduino_Mega2560_Shield" H 7550 3064 60  0000 C CNN
F 2 "Arduino:Arduino_Mega2560_Shield" H 8250 8300 60  0001 C CNN
F 3 "https://store.arduino.cc/arduino-mega-2560-rev3" H 8250 8300 60  0001 C CNN
	1    7550 5550
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male led_pwr1
U 1 1 5CF819A9
P 1850 2800
F 0 "led_pwr1" H 1956 2978 50  0000 C CNN
F 1 "led_pwr" H 1956 2887 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1850 2800 50  0001 C CNN
F 3 "~" H 1850 2800 50  0001 C CNN
	1    1850 2800
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Male ldr1
U 1 1 5CF81A28
P 1850 3550
F 0 "ldr1" H 1956 3828 50  0000 C CNN
F 1 "ldr" H 1956 3737 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 1850 3550 50  0001 C CNN
F 3 "~" H 1850 3550 50  0001 C CNN
	1    1850 3550
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_DIP_x01 DIP1
U 1 1 5CF81AD0
P 2000 4200
F 0 "DIP1" H 2000 4467 50  0000 C CNN
F 1 "SW_DIP_x01" H 2000 4376 50  0000 C CNN
F 2 "Button_Switch_THT:SW_DIP_SPSTx01_Slide_9.78x4.72mm_W7.62mm_P2.54mm" H 2000 4200 50  0001 C CNN
F 3 "" H 2000 4200 50  0001 C CNN
	1    2000 4200
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Male ir_rx1
U 1 1 5CF81B96
P 1850 5250
F 0 "ir_rx1" H 1956 5528 50  0000 C CNN
F 1 "ir_rx" H 1956 5437 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 1850 5250 50  0001 C CNN
F 3 "~" H 1850 5250 50  0001 C CNN
	1    1850 5250
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x05_Male rotary1
U 1 1 5CF81C46
P 1850 6150
F 0 "rotary1" H 1956 6528 50  0000 C CNN
F 1 "rotary" H 1956 6437 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 1850 6150 50  0001 C CNN
F 3 "~" H 1850 6150 50  0001 C CNN
	1    1850 6150
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x07_Male ssd1309
U 1 1 5CF81CCE
P 1850 7150
F 0 "ssd1309" H 1956 7628 50  0000 C CNN
F 1 "display" H 1956 7537 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x07_P2.54mm_Vertical" H 1850 7150 50  0001 C CNN
F 3 "~" H 1850 7150 50  0001 C CNN
	1    1850 7150
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Male relay1
U 1 1 5CF81D8D
P 1900 8100
F 0 "relay1" H 2006 8378 50  0000 C CNN
F 1 "relay" H 2006 8287 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 1900 8100 50  0001 C CNN
F 3 "~" H 1900 8100 50  0001 C CNN
	1    1900 8100
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male rpi_tx1
U 1 1 5CF821E7
P 2900 8850
F 0 "rpi_tx1" H 3006 9028 50  0000 C CNN
F 1 "rpi_tx" H 3006 8937 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2900 8850 50  0001 C CNN
F 3 "~" H 2900 8850 50  0001 C CNN
	1    2900 8850
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Male tx_rx_1
U 1 1 5CF82CCF
P 1500 9200
F 0 "tx_rx_1" H 1606 9478 50  0000 C CNN
F 1 "tx_rx_1" H 1606 9387 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 1500 9200 50  0001 C CNN
F 3 "~" H 1500 9200 50  0001 C CNN
	1    1500 9200
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Male tx_rx_2
U 1 1 5CF82DA5
P 1500 9700
F 0 "tx_rx_2" H 1606 9978 50  0000 C CNN
F 1 "tx_rx_2" H 1606 9887 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 1500 9700 50  0001 C CNN
F 3 "~" H 1500 9700 50  0001 C CNN
	1    1500 9700
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Male tx_rx_3
U 1 1 5CF82E85
P 1500 10200
F 0 "tx_rx_3" H 1606 10478 50  0000 C CNN
F 1 "tx_rx_3" H 1606 10387 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 1500 10200 50  0001 C CNN
F 3 "~" H 1500 10200 50  0001 C CNN
	1    1500 10200
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C1
U 1 1 5CF8AF50
P 2450 4200
F 0 "C1" V 2705 4200 50  0000 C CNN
F 1 "10uF 25V" V 2614 4200 50  0000 C CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 2488 4050 50  0001 C CNN
F 3 "~" H 2450 4200 50  0001 C CNN
	1    2450 4200
	0    -1   -1   0   
$EndComp
Text GLabel 6250 7400 0    50   Input ~ 0
+5V
Text GLabel 6250 6800 0    50   Input ~ 0
GND
Text GLabel 6250 6500 0    50   Input ~ 0
RES
Text GLabel 1700 4200 0    50   Input ~ 0
RES
Text GLabel 2600 4200 2    50   Input ~ 0
GND
Text GLabel 2050 2900 2    50   Input ~ 0
GND
Text GLabel 2050 3650 2    50   Input ~ 0
GND
Text GLabel 2050 3550 2    50   Input ~ 0
+5V
Text GLabel 2050 5350 2    50   Input ~ 0
+5V
Text GLabel 2050 5250 2    50   Input ~ 0
GND
Text GLabel 2050 5950 2    50   Input ~ 0
GND
Text GLabel 2050 6050 2    50   Input ~ 0
+5V
Text GLabel 2050 6850 2    50   Input ~ 0
GND
Text GLabel 2050 6950 2    50   Input ~ 0
+5V
Text GLabel 2100 8000 2    50   Input ~ 0
+5V
Text GLabel 2100 8300 2    50   Input ~ 0
GND
Text GLabel 3650 7550 2    50   Input ~ 0
GND
Text GLabel 3100 8950 2    50   Input ~ 0
GND
Text GLabel 1700 9300 2    50   Input ~ 0
GND
Text GLabel 1700 9800 2    50   Input ~ 0
GND
Text GLabel 1700 10300 2    50   Input ~ 0
GND
Text GLabel 4500 9500 3    50   Input ~ 0
GND
Text GLabel 7600 9200 2    50   Input ~ 0
GND
Text GLabel 8850 3600 2    50   Input ~ 0
LED
Text GLabel 2350 2800 2    50   Input ~ 0
LED
Text GLabel 6250 4700 0    50   Input ~ 0
LDR
Text GLabel 2050 3450 2    50   Input ~ 0
LDR
Text GLabel 8850 5400 2    50   Input ~ 0
IR_RX
Text GLabel 2350 5150 2    50   Input ~ 0
IR_RX
Text GLabel 2050 6150 2    50   Input ~ 0
ROT_BTN_MUTE
Text GLabel 8850 5600 2    50   Input ~ 0
ROT_BTN_MUTE
Text GLabel 2050 6250 2    50   Input ~ 0
ROT_A
Text GLabel 2050 6350 2    50   Input ~ 0
ROT_B
Text GLabel 8850 5800 2    50   Input ~ 0
ROT_A
Text GLabel 8850 6000 2    50   Input ~ 0
ROT_B
Text GLabel 2050 7450 2    50   Input ~ 0
DISP_CS_SS
Text GLabel 8850 7700 2    50   Input ~ 0
DISP_CS_SS
Text GLabel 2050 7350 2    50   Input ~ 0
DISP_DC
Text GLabel 8850 7300 2    50   Input ~ 0
DISP_DC
Text GLabel 2050 7250 2    50   Input ~ 0
DISP_RES
Text GLabel 8850 7100 2    50   Input ~ 0
DISP_RES
Text GLabel 2050 7150 2    50   Input ~ 0
DISP_SDA_MOSI
Text GLabel 8850 7500 2    50   Input ~ 0
DISP_SDA_MOSI
Text GLabel 2050 7050 2    50   Input ~ 0
DISP_SCL_SCK
Text GLabel 8850 7600 2    50   Input ~ 0
DISP_SCL_SCK
Text GLabel 2100 8100 2    50   Input ~ 0
REL1
Text GLabel 8850 3800 2    50   Input ~ 0
REL1
Text GLabel 3950 7950 2    50   Input ~ 0
BTN_PWR
Text GLabel 8850 5200 2    50   Input ~ 0
BTN_PWR
Text GLabel 3950 7850 2    50   Input ~ 0
BTN_IN
Text GLabel 8850 5000 2    50   Input ~ 0
BTN_IN
Text GLabel 3950 7750 2    50   Input ~ 0
BTN_OUT
Text GLabel 8850 4800 2    50   Input ~ 0
BTN_OUT
Text GLabel 3950 7650 2    50   Input ~ 0
BTN_DSP
Text GLabel 8850 4600 2    50   Input ~ 0
BTN_DSP
Text GLabel 4400 9500 3    50   Input ~ 0
+5V
Text GLabel 2000 9100 2    50   Input ~ 0
TX_1
Text GLabel 6250 3700 0    50   Input ~ 0
TX_1
Text GLabel 2000 9200 2    50   Input ~ 0
RX_1
Text GLabel 6250 3600 0    50   Input ~ 0
RX_1
Text GLabel 2000 9600 2    50   Input ~ 0
TX_2
Text GLabel 6250 3900 0    50   Input ~ 0
TX_2
Text GLabel 2000 9700 2    50   Input ~ 0
RX_2
Text GLabel 6250 3800 0    50   Input ~ 0
RX_2
Text GLabel 2000 10100 2    50   Input ~ 0
TX_3
Text GLabel 6250 4100 0    50   Input ~ 0
TX_3
Text GLabel 2000 10200 2    50   Input ~ 0
RX_3
Text GLabel 6250 4000 0    50   Input ~ 0
RX_3
Text GLabel 4600 9800 3    50   Input ~ 0
IN_CTRL_DIGI_A
Text GLabel 6250 4200 0    50   Input ~ 0
IN_CTRL_DIGI_A
Text GLabel 6250 4300 0    50   Input ~ 0
IN_CTRL_DIGI_B
Text GLabel 3400 8850 2    50   Input ~ 0
RPI_TX
Text GLabel 8850 6300 2    50   Input ~ 0
RPI_TX
Text GLabel 6250 6900 0    50   Input ~ 0
GND
Text GLabel 6250 7000 0    50   Input ~ 0
GND
Text GLabel 6250 7100 0    50   Input ~ 0
GND
Text GLabel 6250 7200 0    50   Input ~ 0
GND
Text GLabel 6250 7500 0    50   Input ~ 0
+5V
Text GLabel 6250 7600 0    50   Input ~ 0
+5V
$Comp
L Connector:Conn_01x12_Male J1
U 1 1 5CF95DC2
P 12150 4150
F 0 "J1" H 12256 4828 50  0000 C CNN
F 1 "Conn_01x12_Male" H 12256 4737 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x06_P2.54mm_Vertical" H 12150 4150 50  0001 C CNN
F 3 "~" H 12150 4150 50  0001 C CNN
	1    12150 4150
	1    0    0    -1  
$EndComp
Text GLabel 12350 3650 2    50   Input ~ 0
+5V
Text GLabel 12350 3850 2    50   Input ~ 0
+5V
Text GLabel 12350 4050 2    50   Input ~ 0
+5V
Text GLabel 12350 3750 2    50   Input ~ 0
GND
Text GLabel 12350 3950 2    50   Input ~ 0
GND
Text GLabel 12350 4150 2    50   Input ~ 0
GND
Text GLabel 12350 4350 2    50   Input ~ 0
GND
Text GLabel 12350 4550 2    50   Input ~ 0
GND
Text GLabel 12350 4750 2    50   Input ~ 0
GND
Text GLabel 12350 4250 2    50   Input ~ 0
+3V3
Text GLabel 12350 4450 2    50   Input ~ 0
+3V3
Text GLabel 12350 4650 2    50   Input ~ 0
+3V3
Text GLabel 6250 7300 0    50   Input ~ 0
+3V3
Text GLabel 4700 9800 3    50   Input ~ 0
IN_CTRL_DIGI_B
$Comp
L Connector:Conn_01x04_Male in_sw_digi1
U 1 1 5D00B393
P 4500 9300
F 0 "in_sw_digi1" V 4560 8913 50  0000 R CNN
F 1 "in_sw_digi" V 4651 8913 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 4500 9300 50  0001 C CNN
F 3 "~" H 4500 9300 50  0001 C CNN
	1    4500 9300
	0    -1   1    0   
$EndComp
Text GLabel 2100 8200 2    50   Input ~ 0
REL2
Text GLabel 8850 3700 2    50   Input ~ 0
REL2
$Comp
L Connector:Conn_01x05_Male out_sw1
U 1 1 5DA2C4C8
P 6550 9400
F 0 "out_sw1" H 6656 9778 50  0000 C CNN
F 1 "out_sw" H 6656 9687 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 6550 9400 50  0001 C CNN
F 3 "~" H 6550 9400 50  0001 C CNN
	1    6550 9400
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x05_Male in_sw_analog1
U 1 1 5DA2C8BC
P 7400 9400
F 0 "in_sw_analog1" H 7506 9778 50  0000 C CNN
F 1 "in_sw_analog" H 7506 9687 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 7400 9400 50  0001 C CNN
F 3 "~" H 7400 9400 50  0001 C CNN
	1    7400 9400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5DA4D01F
P 2200 5150
F 0 "R2" V 1993 5150 50  0000 C CNN
F 1 "470R" V 2084 5150 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 2130 5150 50  0001 C CNN
F 3 "~" H 2200 5150 50  0001 C CNN
	1    2200 5150
	0    1    1    0   
$EndComp
$Comp
L Device:R R3
U 1 1 5DA4DB14
P 3800 7950
F 0 "R3" V 3593 7950 50  0000 C CNN
F 1 "470R" V 3684 7950 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 3730 7950 50  0001 C CNN
F 3 "~" H 3800 7950 50  0001 C CNN
	1    3800 7950
	0    1    1    0   
$EndComp
$Comp
L Device:R R1
U 1 1 5DA4DCA7
P 2200 2800
F 0 "R1" V 1993 2800 50  0000 C CNN
F 1 "470R" V 2084 2800 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 2130 2800 50  0001 C CNN
F 3 "~" H 2200 2800 50  0001 C CNN
	1    2200 2800
	0    1    1    0   
$EndComp
$Comp
L Device:R R4
U 1 1 5DA4E057
P 3800 7850
F 0 "R4" V 3593 7850 50  0000 C CNN
F 1 "470R" V 3684 7850 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 3730 7850 50  0001 C CNN
F 3 "~" H 3800 7850 50  0001 C CNN
	1    3800 7850
	0    1    1    0   
$EndComp
$Comp
L Device:R R5
U 1 1 5DA4E1B6
P 3800 7750
F 0 "R5" V 3593 7750 50  0000 C CNN
F 1 "470R" V 3684 7750 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 3730 7750 50  0001 C CNN
F 3 "~" H 3800 7750 50  0001 C CNN
	1    3800 7750
	0    1    1    0   
$EndComp
$Comp
L Device:R R6
U 1 1 5DA4E207
P 3800 7650
F 0 "R6" V 3593 7650 50  0000 C CNN
F 1 "470R" V 3684 7650 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 3730 7650 50  0001 C CNN
F 3 "~" H 3800 7650 50  0001 C CNN
	1    3800 7650
	0    1    1    0   
$EndComp
$Comp
L Device:R R7
U 1 1 5DA4E257
P 3250 8850
F 0 "R7" V 3043 8850 50  0000 C CNN
F 1 "470R" V 3134 8850 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 3180 8850 50  0001 C CNN
F 3 "~" H 3250 8850 50  0001 C CNN
	1    3250 8850
	0    1    1    0   
$EndComp
$Comp
L Device:R R8
U 1 1 5DA4E2AC
P 1850 9100
F 0 "R8" V 1643 9100 50  0000 C CNN
F 1 "470R" V 1734 9100 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 1780 9100 50  0001 C CNN
F 3 "~" H 1850 9100 50  0001 C CNN
	1    1850 9100
	0    1    1    0   
$EndComp
$Comp
L Device:R R9
U 1 1 5DA4E313
P 1850 9200
F 0 "R9" V 1643 9200 50  0000 C CNN
F 1 "470R" V 1734 9200 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 1780 9200 50  0001 C CNN
F 3 "~" H 1850 9200 50  0001 C CNN
	1    1850 9200
	0    1    1    0   
$EndComp
$Comp
L Device:R R10
U 1 1 5DA4E365
P 1850 9600
F 0 "R10" V 1643 9600 50  0000 C CNN
F 1 "470R" V 1734 9600 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 1780 9600 50  0001 C CNN
F 3 "~" H 1850 9600 50  0001 C CNN
	1    1850 9600
	0    1    1    0   
$EndComp
$Comp
L Device:R R11
U 1 1 5DA4E3C0
P 1850 9700
F 0 "R11" V 1643 9700 50  0000 C CNN
F 1 "470R" V 1734 9700 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 1780 9700 50  0001 C CNN
F 3 "~" H 1850 9700 50  0001 C CNN
	1    1850 9700
	0    1    1    0   
$EndComp
$Comp
L Device:R R12
U 1 1 5DA4E416
P 1850 10100
F 0 "R12" V 1643 10100 50  0000 C CNN
F 1 "470R" V 1734 10100 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 1780 10100 50  0001 C CNN
F 3 "~" H 1850 10100 50  0001 C CNN
	1    1850 10100
	0    1    1    0   
$EndComp
$Comp
L Device:R R13
U 1 1 5DA4E474
P 1850 10200
F 0 "R13" V 1643 10200 50  0000 C CNN
F 1 "470R" V 1734 10200 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 1780 10200 50  0001 C CNN
F 3 "~" H 1850 10200 50  0001 C CNN
	1    1850 10200
	0    1    1    0   
$EndComp
$Comp
L Device:R R14
U 1 1 5DA4E4CE
P 4600 9650
F 0 "R14" V 4393 9650 50  0000 C CNN
F 1 "470R" V 4484 9650 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4530 9650 50  0001 C CNN
F 3 "~" H 4600 9650 50  0001 C CNN
	1    4600 9650
	-1   0    0    1   
$EndComp
$Comp
L Device:R R15
U 1 1 5DA4E570
P 4700 9650
F 0 "R15" V 4493 9650 50  0000 C CNN
F 1 "470R" V 4584 9650 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4630 9650 50  0001 C CNN
F 3 "~" H 4700 9650 50  0001 C CNN
	1    4700 9650
	-1   0    0    1   
$EndComp
Text GLabel 6750 9200 2    50   Input ~ 0
GND
Text GLabel 6750 9300 2    50   Input ~ 0
OUT_2-1
Text GLabel 6750 9400 2    50   Input ~ 0
OUT_2-2
Text GLabel 6750 9500 2    50   Input ~ 0
OUT_1-1
Text GLabel 6750 9600 2    50   Input ~ 0
OUT_1-2
Text GLabel 7600 9300 2    50   Input ~ 0
IN1
Text GLabel 7600 9400 2    50   Input ~ 0
IN2
Text GLabel 7600 9500 2    50   Input ~ 0
IN3
Text GLabel 7600 9600 2    50   Input ~ 0
IN4
Text GLabel 8850 4700 2    50   Input ~ 0
IN1
Text GLabel 8850 4900 2    50   Input ~ 0
IN2
Text GLabel 8850 5100 2    50   Input ~ 0
IN3
Text GLabel 8850 5300 2    50   Input ~ 0
IN4
Text GLabel 8850 5500 2    50   Input ~ 0
OUT_2-1
Text GLabel 8850 5700 2    50   Input ~ 0
OUT_2-2
Text GLabel 8850 5900 2    50   Input ~ 0
OUT_1-1
Text GLabel 8850 6100 2    50   Input ~ 0
OUT_1-2
$Comp
L Connector:Conn_01x05_Male front_panel1
U 1 1 5DA19670
P 3450 7750
F 0 "front_panel1" H 3556 8128 50  0000 C CNN
F 1 "front_panel" H 3556 8037 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 3450 7750 50  0001 C CNN
F 3 "~" H 3450 7750 50  0001 C CNN
	1    3450 7750
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male squeezebox1
U 1 1 5DA1BE63
P 3500 5400
F 0 "squeezebox1" H 3606 5578 50  0000 C CNN
F 1 "squeezebox_trigger" H 3606 5487 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 3500 5400 50  0001 C CNN
F 3 "~" H 3500 5400 50  0001 C CNN
	1    3500 5400
	1    0    0    -1  
$EndComp
Text GLabel 3700 5400 2    50   Input ~ 0
GND
Text GLabel 3700 5500 2    50   Input ~ 0
SQUEEZEBOX
Text GLabel 8850 3900 2    50   Input ~ 0
SQUEEZEBOX
$EndSCHEMATC
