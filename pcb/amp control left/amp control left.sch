EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L amp-control-left-rescue:Arduino_Nano_v3.x-MCU_Module A1
U 1 1 5CD7CB24
P 5000 3450
F 0 "A1" V 4954 2410 50  0000 R CNN
F 1 "Arduino_Nano_v3.x" V 5045 2410 50  0000 R CNN
F 2 "Module:Arduino_Nano" H 5150 2500 50  0001 L CNN
F 3 "http://www.mouser.com/pdfdocs/Gravitech_Arduino_Nano3_0.pdf" H 5000 2450 50  0001 C CNN
	1    5000 3450
	0    1    1    0   
$EndComp
$Comp
L Device:R R1
U 1 1 5CD7DA4F
P 6300 2450
F 0 "R1" H 6370 2496 50  0000 L CNN
F 1 "750 Ohm +/- 10%" H 6370 2405 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Vertical" V 6230 2450 50  0001 C CNN
F 3 "~" H 6300 2450 50  0001 C CNN
	1    6300 2450
	1    0    0    -1  
$EndComp
NoConn ~ 6000 3550
NoConn ~ 5100 2950
NoConn ~ 5000 2950
NoConn ~ 4900 2950
NoConn ~ 4800 2950
NoConn ~ 4700 2950
NoConn ~ 4600 2950
NoConn ~ 4400 2950
NoConn ~ 4300 2950
NoConn ~ 4000 3550
NoConn ~ 5000 3950
NoConn ~ 4400 3950
NoConn ~ 4500 3950
NoConn ~ 4600 3950
NoConn ~ 4700 3950
NoConn ~ 4800 3950
NoConn ~ 4900 3950
NoConn ~ 5200 3950
NoConn ~ 5600 3950
$Comp
L Connector:Conn_01x12_Male J1
U 1 1 5CD86407
P 5050 1500
F 0 "J1" V 4885 1427 50  0000 C CNN
F 1 "Conn_01x12_Male" V 4976 1427 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x12_P2.54mm_Vertical" H 5050 1500 50  0001 C CNN
F 3 "~" H 5050 1500 50  0001 C CNN
	1    5050 1500
	0    1    1    0   
$EndComp
Text GLabel 5550 1700 3    50   Input ~ 0
VIN
Text GLabel 5450 1700 3    50   Input ~ 0
GND
Text GLabel 4650 1700 3    50   Input ~ 0
GND
Text GLabel 5050 1700 3    50   Input ~ 0
GND
Text GLabel 4750 1700 3    50   Input ~ 0
GND
Text GLabel 5250 1700 3    50   Input ~ 0
GND
Text GLabel 5350 1700 3    50   Input ~ 0
PowerSwitch
Text GLabel 6300 2600 3    50   Input ~ 0
PowerLed
Text GLabel 4850 1700 3    50   Input ~ 0
5V
Text GLabel 4950 1700 3    50   Input ~ 0
Relay
Text GLabel 4550 1700 3    50   Input ~ 0
RX
Text GLabel 4450 1700 3    50   Input ~ 0
TX
Text GLabel 6000 3350 2    50   Input ~ 0
VIN
Text GLabel 4000 3450 0    50   Input ~ 0
GND
Text GLabel 5500 2950 1    50   Input ~ 0
TX
Text GLabel 5600 2950 1    50   Input ~ 0
RX
Text GLabel 5400 2950 1    50   Input ~ 0
PowerSwitch
Text GLabel 4500 2950 1    50   Input ~ 0
PowerLed
Text GLabel 6000 3650 2    50   Input ~ 0
5V
Text GLabel 5200 2950 1    50   Input ~ 0
Relay
Text GLabel 6300 2300 1    50   Input ~ 0
PowerLedR
Text GLabel 5150 1700 3    50   Input ~ 0
PowerLedR
NoConn ~ 5300 2950
$Comp
L Connector:Conn_01x03_Male J2
U 1 1 5CE80C31
P 3750 1500
F 0 "J2" V 3810 1640 50  0000 L CNN
F 1 "Conn_01x03_Male" V 3901 1640 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 3750 1500 50  0001 C CNN
F 3 "~" H 3750 1500 50  0001 C CNN
	1    3750 1500
	0    1    1    0   
$EndComp
Text GLabel 3750 1700 3    50   Input ~ 0
5V
Text GLabel 3850 1700 3    50   Input ~ 0
GND
Text GLabel 3650 1700 3    50   Input ~ 0
LDR_S
Text GLabel 4300 3950 3    50   Input ~ 0
LDR_S
$Comp
L Device:CP C1
U 1 1 5CE828CA
P 3200 4750
F 0 "C1" V 2945 4750 50  0000 C CNN
F 1 "10 uF" V 3036 4750 50  0000 C CNN
F 2 "Capacitor_THT:CP_Radial_D4.0mm_P1.50mm" H 3238 4600 50  0001 C CNN
F 3 "~" H 3200 4750 50  0001 C CNN
	1    3200 4750
	0    1    1    0   
$EndComp
$Comp
L Switch:SW_DIP_x01 SW1
U 1 1 5CE82950
P 3650 4750
F 0 "SW1" H 3650 5017 50  0000 C CNN
F 1 "SW_DIP_x01" H 3650 4926 50  0000 C CNN
F 2 "Button_Switch_THT:SW_DIP_SPSTx01_Slide_6.7x4.1mm_W7.62mm_P2.54mm_LowProfile" H 3650 4750 50  0001 C CNN
F 3 "" H 3650 4750 50  0001 C CNN
	1    3650 4750
	1    0    0    -1  
$EndComp
Text GLabel 3050 4750 0    50   Input ~ 0
GND
Text GLabel 3950 4750 2    50   Input ~ 0
RST
Text GLabel 5500 3950 3    50   Input ~ 0
RST
$EndSCHEMATC
