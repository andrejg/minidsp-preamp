## Development branch ideas & todo

* Load/save Arduino state to RasPI
  - tracked states:
    - volume
    - dirac state
    - selected input
    - selected output/profile
  - load state from RasPI at boot
  - save state to RasPI when tracked state changes
* Load/save configuration to RasPI
  - load config at boot
    1. press power button
    2. arduino turns on minidsp & raspi
    3. raspi sends config data to arduino
    4. arduino parses config data and initializes
  - save config when settings have changed
    - just send delta, not the whole config?
  - configuration contents:
    - input names
    - default input volume
    - output names
    - default output volume
    - all strings?
    - ir rx codes?
      - ir remote programming?
    - source_states struct
    - output_states struct
    - minidsp logo
    - mute symbol
    - power save timeout
* configuration and states in xaml? or json?
* implement menu control
  - rotary press: enter menu
  - rotary left/right controls menu
  - rotary press when in menu: select/toggle
  - menu control w/ ir remote?
  - menu structure:
    - power amplifiers
      - left toggle on/off
      - right toggle on/off
      - sub toggle on/off
    - squeezelite start/stop
    - start dirac calibration
    - inputs
      - set name, default volume
    - outputs
      - set name, default volume
    - system
      - display ip/hostname
      - save configuration
      - load configuration
      - reboot (RasPi)
    - power off
    - exit
* REST API in Python (+ HomeAssistant/Node-RED integration?)
* Ansible(?) script for deploying to RasPI
* isolation of serial connections to poweramp arduinos
  - replace arduinos w/ esp32 & switch to wifi?
* replace arduino mega w/ esp32 & run the rest api from there?
* better power supply
  - 5V/~5A for RasPi
  - ~9V for Arduino
  - 12V for MiniDSP
  - Arduino always on
  - linear regulated PSU kits + transformers from audiophonics.fr or jimsaudio.com? & custom enclosure?
  - voltage & current monitoring (& control?) w/ another arduino nano?
  - MiniDSP & RasPi w/ (solid state?) relays on AC (before their PSUs) instead on DC lines