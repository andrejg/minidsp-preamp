# MiniDSP DDRC-24 USB Command codes
__Unmute:__<br/>
03 17 00 1a<br/>
_Response:_<br/>
01 17 00 10<br/>

__Mute:__<br/>
03 17 00 1b<br/>
_Response:_<br/>
01 17 01 10<br/>

__Dirac off:__<br/>
03 3f 01 43<br/>
_Response:_<br/>
01 3f 01 10<br/>

__Dirac on:__<br/>
03 3f 00 42<br/>
_Response:_<br/>
01 3f 00 10<br/>

__Set input to analog:__<br/>
03 34 00 37<br/>
_Response:_<br/>
01 34 00 10<br/>
05 05 ff a9<br/>

__Set input to toslink:__<br/>
03 34 01 38<br/>
_Response:_<br/>
01 34 01 10<br/>
05 05 ff a9<br/>

__Set input to USB:__<br/>
03 34 02 39<br/>
_Response:_<br/>
01 34 02 10<br/>
05 05 ff a9<br/>

__Activate config 1:__<br/>
04 25 00 02 2b ff<br/>
05 05 ff e5 01 ef<br/>
05 05 ff e0 01 ea<br/>
05 05 ff da 02 e5<br/>
07 45 a1 03 00 00 02 f2<br/>
_Response:_<br/>
02 ab 00 02 3e 59 03 c3 3e 59 03 c3<br/>

__Activate config 2:__<br/>
04 25 01 02 2c ff<br/>
05 05 ff e5 01 ef<br/>
05 05 ff e0 01 ea<br/>
05 05 ff da 02 e5<br/>
07 45 a1 03 00 00 02 f2<br/>
_Response:_<br/>
02 ab 01 02 43 68 03 c3 43 68 03 c3<br/>

__Activate config 3:__<br/>
04 25 02 02 2d ff<br/>
05 05 ff e5 01 ef<br/>
05 05 ff e0 01 ea<br/>
05 05 ff da 02 e5<br/>
07 45 a1 03 00 00 02 f2<br/>
_Response:_<br/>
02 ab 02 02 54 56 03 c3 54 56 03 c3<br/>

__Activate config 4:__<br/>
04 25 03 02 2e ff<br/>
05 05 ff e5 01 ef<br/>
05 05 ff e0 01 ea<br/>
05 05 ff da 02 e5<br/>
07 45 a1 03 00 00 02 f2<br/>
_Response:_<br/>
02 ab 03 02 ab 67 03 c3 ab 67 03 c3<br/>

__Set volume:__<br/>
03 42 {vol} cf<br/>
_Response:_<br/>
01 42 {vol} 10<br/>
_Notes:_<br/>
{vol} = desired volume (-127.5 to 0 dB) * 2 and converted to HEX.<br/>

__Get input levels:__<br/>
05 14 00 10 02 2b<br/>
_Response:_<br/>
0c 14 00 10 {xx} {xx} {yy} {yy} {aa} {aa} {bb} {bb}<br/>
{yy} {yy}: Input 1 level<br/>
{bb} {bb}: Input 2 level<br/>
{xx} and {aa}: Unknown<br/>

__Master volume and Mute:__<br/>
05 05 ff da 02 e5<br/>
_Response:_<br/>
06 05 ff da __{vol}__ __{mute}__<br/>
_Note:_<br/>
Volume in dB = vol * -0.5<br/>
Mute = {0 for unmuted , 1 for muted}<br/>

__Preset/config selection:__<br/>
05 05 ff d8 01 e2<br/>
_Response:_<br/>
05 05 ff d8 __{preset number}__<br/>

__Input source selected:__<br/>
05 05 ff d9 01 e3<br/>
_Response:_<br/>
05 05 ff d9 __{source number}__<br/>

__Dirac status:__<br/>
05 05 ff e0 01 ea<br/>
_Response:_<br/>
05 05 ff e0 __{status}__<br/>
_Note:_<br/>
status = {0 for On, 1 for Off}