String inputStringSerial0 = "";
bool inputStringSerial0Complete = false;

String inputStringSerial1 = "";
bool inputStringSerial1Complete = false;

// serial1 trs:
// tip:     rx
// ring:    tx
// sleeve;  gnd

void setup() {
  Serial.begin(2400);
  Serial1.begin(2400);
}

void loop() {
  if (Serial1.available()) Serial1Event();
}

void Serial0Event() {
  while (Serial.available()) {
    char inChar = (char)Serial.read();
    inputStringSerial0 += inChar;

    if (inChar == '\n') {
      inputStringSerial0Complete = true;
    }

    if (inputStringSerial0Complete)
    {
      inputStringSerial0.replace("\n", "");
      if (inputStringSerial0 == "D") {
        Serial1.write("D\n");
      }
      
      // cleanup
      inputStringSerial0 = "";
      inputStringSerial0Complete = false;
    }
  }
}

void Serial1Event() {
  while (Serial1.available()) {
    char inChar = (char)Serial1.read();
    inputStringSerial1 += inChar;
    if (inChar == '\n') {
      inputStringSerial1Complete = true;
    }

    if (inputStringSerial1Complete) {
      // echo string from Serial1 to Serial0
      Serial.print(inputStringSerial1);
      // cleanup
      inputStringSerial1 = "";
      inputStringSerial1Complete = false;
    }
  }
}

// serial0 events
void serialEventRun(void) {
  if (Serial.available()) Serial0Event();
}
