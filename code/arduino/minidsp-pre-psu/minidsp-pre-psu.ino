// control the 5/9/12V PSU for the preamp over serial port
// TRS wiring for the serial port:
// Tip:     RX
// Ring:    TX
// Sleeve:  GND

#include <elapsedMillis.h>

// LED
#define led 11
int ledValue = 32;

// Relay
#define rel 4

// State variables
int statePwr = 0;
int stateRel = 0;

elapsedMillis ledBlinkElapsed;

void setup() {
  pinMode(led, OUTPUT);
  pinMode(rel, OUTPUT);

  // turn relays off. HIGH = off!
  digitalWrite(rel, HIGH);

  Serial.begin(9600);
}

void loop() {
  if (statePwr == 0) {
    if (ledBlinkElapsed >= 1500) {
      analogWrite(led, ledValue);
      delay(25);
      digitalWrite(led, 0);
      ledBlinkElapsed = 0;
    }
  }
  else if (statePwr == 1) {
    analogWrite(led, ledValue);
  }
}

void notifySender(char str[]) {
  Serial.write(str);
}

void powerUp() {
  digitalWrite(rel, LOW);
  statePwr = 1;
  stateRel = 1;

  char str5V[10] = "PSU-005-";
  itoa(statePwr, str5V + 8, 10); 
  strcat(str5V, "\n");

  notifySender(str5V);

  char str12V[10] = "PSU-012-";
  itoa(statePwr, str12V + 8, 10);
  strcat(str12V, "\n");

  notifySender(str12V);
}

void powerDown() {
  digitalWrite(rel, HIGH);
  statePwr = 0;
  stateRel = 0;
  
  char str5V[10] = "PSU-005-";
  itoa(statePwr, str5V + 8, 10); 
  strcat(str5V, "\n");

  notifySender(str5V);

  char str12V[10] = "PSU-012-";
  itoa(statePwr, str12V + 8, 10);
  strcat(str12V, "\n");

  notifySender(str12V);
}

void serialEventRun(void) {
  if (Serial.available()) {
    char inStr[21];
    int idxSerial = 0;
    delay(21);
    serialEvent(inStr, idxSerial);
  }
}

void serialEvent(char inStr[], int idxSerial)
{
  bool stringComplete = false;
  char delimiter = '\n';
  
  while (Serial.available()) {
    inStr[idxSerial] = Serial.read();
    if (inStr[idxSerial] == delimiter) {
      stringComplete = true;
    }
    idxSerial++;
  }
  inStr[idxSerial] = '\0';

  if (stringComplete) {
    stringComplete = false;

    serialEventTokenize(inStr);
  }
}

void serialEventTokenize(char inStr[]) {
  char *cmd = strtok(inStr, "\n");

  while (cmd) {
    serialEventExecute(cmd);
    cmd = strtok(NULL, "\n");
  }
}

void serialEventExecute(char cmd[]) {
  // BLINK
  // sync blink w/ MCU on preamp & poweramp
  if (!strcmp(cmd, "BLINK")) {
    ledBlinkElapsed = 1500;
  }
  // 0 - power off
  if (!strcmp(cmd, "0")) {
    powerDown();
  }
  // 1 - power up
  if (!strcmp(cmd, "1")) {
    powerUp();
  }
  // Lxxx - LED level
  if (startsWith("L", cmd) == 1) {
    memmove(cmd, cmd+1, strlen(cmd));
    ledValue = atoi(cmd);
  }
}

// check if string starts with prefix
int startsWith(const char *pre, const char *str) {
  return strncmp(pre, str, strlen(pre)) == 0;
}
