// poweramp left

#include <RBD_Timer.h>
#include <RBD_Button.h>
#include <elapsedMillis.h>

#define led 11
RBD::Button btn_pwr(2);
#define relay 4
#define ldr A7

int state_pwr = 0;

int ldr_value = 0;
int led_value = 0;

elapsedMillis led_blink_elapsed;

void setup() {
  pinMode(led, OUTPUT);
  pinMode(relay, OUTPUT);

  digitalWrite(led, LOW);
  digitalWrite(relay, LOW);

  state_pwr = 0;

  Serial.begin(9600);
}

void loop() {

  // always monitor ambient light levels
  ldr_value = (analogRead(ldr))/4;
  led_value = 255 - (ldr_value > 254 ? 254 : ldr_value);
  
  if (btn_pwr.onPressed()) {
    switch(state_pwr) {
      case 0: {
        power_on();
        break;
      }
      case 1: {
        power_off();
        break;
      }
    }
  }

  if (state_pwr == 0) {
    // blink power led every 1.5s when in standby
    if (led_blink_elapsed >= 1500) {
      analogWrite(led, led_value);
      delay(25);
      digitalWrite(led, 0);
      led_blink_elapsed = 0;
    }
  }
  else if (state_pwr == 1) {
    analogWrite(led, led_value);
  }
}

void power_on() {
  digitalWrite(led, HIGH);
  digitalWrite(relay, HIGH);
  Serial.write("ICE1\n");
  state_pwr = 1;
}

void power_off() {
  digitalWrite(led, LOW);
  digitalWrite(relay, LOW);
  Serial.write("ICE0\n");
  state_pwr = 0;
}

# pragma region serial communication handling
void serialEventRun(void) {
  if (Serial.available()) {
    char inStr[20];
    int idxInStr = 0;
    delay(10);
    serialEvent(inStr, idxInStr);
  }
}

void serialEvent(char inStr[], int idxInStr) {
  bool inStrComplete = false;
  char delimiter = '\n';

  while (Serial.available()) {
    inStr[idxInStr] = Serial.read();
    if (inStr[idxInStr] == delimiter) {
      inStrComplete = true;
    }
    idxInStr++;
  }
  inStr[idxInStr] = '\0';

  if (inStrComplete) {
    inStrComplete = false;

    serialEventTokenize(inStr);
  }
}

void serialEventTokenize(char inStr[]) {
  char *cmd = strtok(inStr, "\n");

  while (cmd) {
    serialEventExecute(cmd);
    cmd = strtok(NULL, "\n");
  }
}

void serialEventExecute(char cmd[]) {
  // BLINK
  // sync blink w/ MCU on preamp.
  if (!strcmp(cmd, "BLINK")) {
    led_blink_elapsed = 1500;
  }
  // 0 - power off
  if (!strcmp(cmd, "0")) {
    for (int i = 0; i <= 1; i++) {
      digitalWrite(led, HIGH);
      delay(100);
      digitalWrite(led, LOW);
      delay(100);
    }
    power_off();
  }
  
  // 1 - power on
  if (!strcmp(cmd, "1")) {
    for (int i = 0; i <= 1; i++) {
      digitalWrite(led, HIGH);
      delay(100);
      digitalWrite(led, LOW);
      delay(100);
    }
    power_on();
  }
}
# pragma endregion
