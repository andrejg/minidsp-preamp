/// load libraries
#include <MD_REncoder.h>
#include <IRremote.h>
#include <RBD_Button.h>
#include <RBD_Timer.h>
#include <U8g2lib.h>
#include <U8x8lib.h>
#include <elapsedMillis.h>

//#define DEBUG 1

// define rpi tx pin
#define rpi_tx 39

#pragma region infrared
// define ir receiver pin
#define ir_rx 30

// define ir rx codes (minidsp remote)
const unsigned long rx_minidsp_source =   0xF10ECA35; // 4044278325
const unsigned long rx_minidsp_prof_1 =   0xFE01CA35; // 4261530165
const unsigned long rx_minidsp_prof_2 =   0xFD02CA35; // 4244818485
const unsigned long rx_minidsp_prof_3 =   0xFA05CA35; // 4194683445
const unsigned long rx_minidsp_prof_4 =   0xFF00CA35; // 4278241845
const unsigned long rx_minidsp_dsp =      0xF906CA35; // 4177971765
const unsigned long rx_minidsp_vol_up =   0xF50ACA35; // 4111125045
const unsigned long rx_minidsp_vol_down = 0xF609CA35; // 4127836725
const unsigned long rx_minidsp_mute =     0xF30CCA35; // 4077701685

// define ir rx codes (apple remote) 
const unsigned long rx_apple_vol_up =     0x9D0A87EE; // 2634713070
const unsigned long rx_apple_vol_down =   0x9D0C87EE; // 2634844142
const unsigned long rx_apple_prev =       0x9D0987EE; // 2634647534
const unsigned long rx_apple_next =       0x9D0687EE; // 2634450926
const unsigned long rx_apple_play =       0x9D0587EE; // 2634385390
const unsigned long rx_apple_menu =       0x9D0387EE; // 2634254318

// rx code when btn held:
const unsigned long rx_held =             0x00000000;

unsigned long previous_ir_command = 0;
int rx_held_cycles = 0;
float volume_step = 0.5;

decode_results ir_res;

// ir variables
unsigned int cmd_interval = 500;
elapsedMillis timeSinceLastIR;
elapsedMillis cmd_elapsed;
elapsedMillis led_blink_elapsed;
#pragma endregion

#pragma region rotary encoder
// define rotary pins
#define rot_a 34
#define rot_b 36
#define DEFAULT_PERIOD 100 // sampling period for rotary speed calculation

// define md_rencoder
MD_REncoder rot = MD_REncoder(rot_a, rot_b);
#pragma endregion

#pragma region oled
// define oled pins
#define oled_scl 52
#define oled_sda 51
#define oled_rst 47
#define oled_dc 49
#define oled_cs 53

// define oled
U8G2_SSD1309_128X64_NONAME0_F_4W_SW_SPI u8g2(U8G2_R0, oled_scl, oled_sda, oled_cs, oled_dc, oled_rst);

// oled variables
bool oled_refresh = false;
bool state_oled = true;
int oled_brightness = 0;
unsigned int inactive_interval = 60000;
elapsedMillis oled_inactive_elapsed = 0;
#pragma endregion

#pragma region input
// CD4052B:
// A  B  out
// 0  0  x0, y0
// 1  0  x1, y1
// 0  1  x2, y2
// 1  1  x3, y3

// define digital input mux pins
#define in_ctrl_digi_a 20
#define in_ctrl_digi_b 21

// define analog input relay pins
#define in_ctrl_analog_1 23
#define in_ctrl_analog_2 25
#define in_ctrl_analog_3 27
#define in_ctrl_analog_4 29

// define input switch button
RBD::Button btn_switch_input(26);

// define input states
typedef struct {
  int current_src;
  int next_src;
  int prev_src;
  int in_analog_1;
  int in_analog_2;
  int in_analog_3;
  int in_analog_4;
  int in_digi_a;
  int in_digi_b;
  float target_volume; // > 0: not set, as volume range is -127.5 to 0 dB
  int minidsp_input; // 1 - toslink, 2 - analog, 3 - usb
  String pi_command;
  String display_text;
} source_state;

source_state source_states[9] = {
//|c||n||p||a||a||a||a||t||t| |v| |m| |pi|  |text|
  {0, 1, 8, 0, 0, 0, 0, 0, 0, -35, 1, "I1", "TOSLINK 1"},
  {1, 2, 0, 0, 0, 0, 0, 1, 0, -35, 1, "I1", "TOSLINK 2"},
  {2, 3, 1, 0, 0, 0, 0, 0, 1, -35, 1, "I1", "TOSLINK 3"},
  {3, 4, 2, 0, 0, 0, 0, 1, 1, -35, 1, "I1", "TOSLINK 4"},
  {4, 5, 3, 1, 0, 0, 0, 0, 0, -20, 2, "I2", "ANALOG 1"},
  {5, 6, 4, 0, 1, 0, 0, 0, 0, -35, 2, "I2", "ANALOG 2"},
  {6, 7, 4, 0, 0, 1, 0, 0, 0, -35, 2, "I2", "ANALOG 3"},
  {7, 8, 6, 0, 0, 0, 1, 0, 0, -35, 2, "I2", "ANALOG 4"},
  {8, 0, 7, 0, 0, 0, 0, 0, 0, -35, 3, "I3", "USB"}
};
#pragma endregion

#pragma region output

// define output mux relay pins
#define out_ctrl_11 35
#define out_ctrl_12 37
#define out_ctrl_21 31
#define out_ctrl_22 33

// define output switch button
RBD::Button btn_switch_output(24);

// define output states
typedef struct {
  int current_out;
  int next_out;
  int prev_out;
  int out_11;
  int out_12;
  int out_21;
  int out_22;
  int dsp_state;
  int minidsp_profile;
  int amplifier1; // icepower, via Serial2
  int amplifier2; // unused, via Serial3
  int iec1; // self, on IEC1. always on
  int iec2; // subwoofer, via Serial1 IEC2
  int iec3; // hedphones, via Serial1 IEC3
  String pi_command;
  String display_text;
} output_state;

output_state output_states[4] = {
  {0, 1, 3, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 0, "P1", "SPK/HARM"}, // 1-1 2-1 on, 1-2 2-2 off; icepower + sub, harman curve
  {1, 2, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 0, "P2", "SPK/NAD"}, // 1-1 2-1 on, 1-2 2-2 off; icepower + sub, nad curve
  {2, 3, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 0, "P3", "SPK/DIR"}, // 1-1 2-1 on, 1-2 2-2 off; icepower + sub, dirac default curve
  {3, 0, 2, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 0, "P4", "SPK/NA"}, // 1-1 2-1 on, 1-2 2-2 off; unused
};

#pragma endregion

#pragma region dsp
// define dsp toggle button pin
RBD::Button btn_dsp(22);

// define dsp states
typedef struct {
  int current_dsp;
  int next_dsp;
  String pi_command;
  String display_text;
} dsp_state;

dsp_state dsp_states[2] = {
  {0, 1, "D0", "off"},
  {1, 0, "D1", "Live"}
};

// dsp variables
elapsedMillis btn_dsp_elapsed;
bool btn_dsp_pressed = false;
#pragma endregion

#pragma region mute
// define mute button pin
RBD::Button btn_mute(32);

// define mute states
typedef struct {
  int current_mute;
  int next_mute;
  String pi_command;
  String display_text;
} mute_state;

mute_state mute_states[2] = {
  {0, 1, "M0", ""}, // unmuted
  {1, 0, "M1", "mute"} // muted
};

// mute variables
elapsedMillis btn_mute_elapsed = 0;
elapsedMillis rx_mute_elapsed = 0;
bool btn_mute_pressed = false;
#pragma endregion

#pragma region power
// define power button pin
RBD::Button btn_pwr(28);

// define power led pin
#define led_pwr 4

// define light sensor pin
#define ldr A0

// define power states
typedef struct {
  int current_pwr;
  int next_pwr;
  int rel;
  String pi_command;
} power_state;

power_state power_states[2] = {
  {0, 1, 0, "S"},
  {1, 0, 1, ""}
};
#pragma endregion

#pragma region graphics
// splash logo
#define u8g_minidsp_logo_width 128
#define u8g_minidsp_logo_height 21
static unsigned char u8g_minidsp_logo_bits[] = {
   0x00, 0x00, 0x00, 0x00, 0x00, 0x06, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0f, 0x00, 0x80,
   0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x0f, 0x00, 0x80, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x06, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3f, 0x00, 0xf8,
   0x01, 0x06, 0xf8, 0x00, 0x03, 0xf8, 0xff, 0x00, 0xff, 0x1f, 0xff, 0x1f,
   0x80, 0x7f, 0x00, 0xfc, 0x03, 0x0f, 0xfc, 0x81, 0x07, 0xfc, 0xff, 0xc1,
   0xff, 0xbf, 0xff, 0x7f, 0xc0, 0xff, 0x00, 0xfe, 0x07, 0x0f, 0xfe, 0x83,
   0x07, 0xfc, 0xff, 0xe3, 0xff, 0x1f, 0xff, 0x7f, 0xe0, 0xff, 0x01, 0xff,
   0x0f, 0x0f, 0xff, 0x87, 0x07, 0x1c, 0x80, 0xe7, 0x03, 0x00, 0x00, 0xf8,
   0xe0, 0xe1, 0x01, 0x0f, 0x0f, 0x0f, 0x8f, 0x87, 0x07, 0x1c, 0x00, 0xe7,
   0x00, 0x00, 0x00, 0xe0, 0xf0, 0xc0, 0x83, 0x07, 0x1e, 0x8f, 0x07, 0x8f,
   0x07, 0x1c, 0x00, 0xe7, 0x00, 0x00, 0x00, 0xe0, 0xf0, 0xc0, 0x83, 0x07,
   0x1e, 0x8f, 0x07, 0x8f, 0x07, 0x1c, 0x00, 0xe7, 0x03, 0x00, 0x00, 0xf8,
   0x70, 0x80, 0x83, 0x03, 0x1c, 0x8f, 0x03, 0x8e, 0x07, 0x1c, 0x00, 0xe7,
   0xff, 0x0f, 0xfe, 0x7f, 0x78, 0x80, 0xc7, 0x03, 0x3c, 0xcf, 0x03, 0x9e,
   0x07, 0x1c, 0x00, 0xc7, 0xff, 0x1f, 0xff, 0x7f, 0x78, 0x80, 0xc7, 0x03,
   0x3c, 0xcf, 0x03, 0x9e, 0x07, 0x1c, 0x00, 0x07, 0xff, 0x1f, 0xff, 0x1f,
   0x38, 0x00, 0xef, 0x01, 0x78, 0xe6, 0x01, 0x3c, 0x03, 0x1c, 0x00, 0x07,
   0x00, 0x1c, 0x07, 0x00, 0x3c, 0x00, 0xef, 0x01, 0x78, 0xe0, 0x01, 0x3c,
   0x00, 0x00, 0x00, 0x07, 0x00, 0x1c, 0x07, 0x00, 0x1c, 0x00, 0xfe, 0x00,
   0xf0, 0xf0, 0x00, 0x78, 0x00, 0x00, 0x80, 0x07, 0x00, 0x1c, 0x07, 0x00,
   0x1e, 0x00, 0xfe, 0x00, 0xf0, 0xff, 0x00, 0xf8, 0xc3, 0xff, 0xff, 0xc3,
   0xff, 0x1f, 0x07, 0x00, 0x0f, 0x00, 0x7c, 0x00, 0xe0, 0x7f, 0x00, 0xf0,
   0xe7, 0xff, 0xff, 0xe1, 0xff, 0x0f, 0x07, 0x00, 0x07, 0x00, 0x38, 0x00,
   0x80, 0x1f, 0x00, 0xc0, 0xc3, 0xff, 0xff, 0xc0, 0xff, 0x07, 0x02, 0x00 };

// mute symbol
#define u8g_mute_width 26
#define u8g_mute_height 22
static unsigned char u8g_mute_bits[] = {
   0x00, 0x00, 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x1c, 0x00, 0x00,
   0x00, 0x1e, 0x00, 0x00, 0x00, 0x1f, 0x00, 0x00, 0x80, 0x1f, 0x81, 0x00,
   0xc0, 0x9f, 0xc3, 0x01, 0xfe, 0xdf, 0xe7, 0x03, 0xff, 0x9f, 0xff, 0x01,
   0xff, 0x1f, 0xff, 0x00, 0xff, 0x1f, 0x7e, 0x00, 0xff, 0x1f, 0x7e, 0x00,
   0xff, 0x1f, 0xff, 0x00, 0xff, 0x9f, 0xff, 0x01, 0xfe, 0xdf, 0xe7, 0x03,
   0xc0, 0x9f, 0xc3, 0x01, 0x80, 0x1f, 0x81, 0x00, 0x00, 0x1f, 0x00, 0x00,
   0x00, 0x1e, 0x00, 0x00, 0x00, 0x1c, 0x00, 0x00, 0x00, 0x18, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00 };

// graphics variables
bool splash_played = false;
#pragma endregion

#pragma region variables
// state variables. 0 - off, 1 - on
int state_pwr = 0;
int state_rpi = 0;
int state_mute = 0; // 0-unmute, 1-mute
int state_dsp = 1;
int state_src = 0; // 0-8 - see source_states
int state_out = 0; // 0, 1 - see output_states
float volume = 0;
bool is_calibrating = false;

// psu states
int state_5v = 0;
int state_12v = 0;
int state_iec1 = 0;
int state_iec2 = 0;
int state_iec3 = 0;

// amplifier state(s)
int state_icepower = 0;
int state_amp2 = 0;

// display variables
String txt_src = "";
String txt_out = "";
String txt_vol = "";
String txt_mute = "";
String txt_dsp = "";
String txt_dsp_static = "Dirac";
String local_ip = "";
String local_hostname = "";
bool blink = true;

// light sensor variables
int ldr_value = 0;
int led_pwr_value = 0;

unsigned int psuLDRInterval = 1500;
elapsedMillis psuLDRElapsed = 0;

// serial communication variables
char delimiter = '\n';

#pragma endregion

void setup() {
  /// initialize ir input
  IrReceiver.begin(ir_rx, false);
  
  /// initialize rotary
  rot.begin();
  
  /// set pin modes
  pinMode(rot_a, INPUT_PULLUP);
  pinMode(rot_b, INPUT_PULLUP);
  pinMode(ir_rx, INPUT_PULLUP);
  pinMode(led_pwr, OUTPUT);
  pinMode(in_ctrl_digi_a, OUTPUT);
  pinMode(in_ctrl_digi_b, OUTPUT);
  pinMode(in_ctrl_analog_1, OUTPUT);
  pinMode(in_ctrl_analog_2, OUTPUT);
  pinMode(in_ctrl_analog_3, OUTPUT);
  pinMode(in_ctrl_analog_4, OUTPUT);
  pinMode(out_ctrl_11, OUTPUT);
  pinMode(out_ctrl_12, OUTPUT);
  pinMode(out_ctrl_21, OUTPUT);
  pinMode(out_ctrl_22, OUTPUT);
  pinMode(ldr, INPUT);
  pinMode(rpi_tx, INPUT); // *** DON'SET TO OUTPUT ***
  
  /// initialize serial (+ serial1, serial2 & serial3 for triggers)
  Serial.begin(115200);   // raspberry pi
  Serial1.begin(9600);    // iec/power strip controller
  Serial2.begin(9600);    // icepower
  Serial3.begin(9600);    // 5V, 9V, 12V psu

  u8g2.begin();
  u8g2.setPowerSave(0);

  for (int i = 0; i <= 4; i++) {
    digitalWrite(led_pwr, HIGH);
    delay(100);
    digitalWrite(led_pwr, LOW);
    delay(100);
  }
}

void loop() {
  // always monitor ambient light
  ldr_value = (analogRead(ldr))/4;
  led_pwr_value = 255 - (ldr_value > 254 ? 254 : ldr_value);

  // send LED value to PSU every 1.5s
  if (psuLDRElapsed >= psuLDRInterval) {
    char strLED[6] = "L";
    itoa(led_pwr_value, strLED + 1, 10);
    strcat(strLED, "\n");
    Serial3.write(strLED);
    psuLDRElapsed = 0;
  }
  
  if (btn_pwr.onPressed()) {
    if (state_pwr == 1) {
      // shutdown
      power_down();
    }
    else if (state_pwr == 0) {
      // power up
      power_up();
    }
  }
  
  if (state_pwr == 0 && state_rpi == 0) {
    // blink power led every 1.5s when in standby
    if (led_blink_elapsed >= 1500) {
      analogWrite(led_pwr, led_pwr_value);
      delay(25);
      digitalWrite(led_pwr, 0);
      led_blink_elapsed = 0;
      // sync LEDs
      Serial1.write("BLINK\n");
      Serial2.write("BLINK\n");
      Serial3.write("BLINK\n");
    }
  }

  if (state_pwr == 1 && state_rpi == 0) {
    // powered on, waiting for pi

    u8g2.setPowerSave(0); // wake up display
    u8g2.firstPage();
    do {
      u8g2.setFont(u8g2_font_micro_tr);
      String loading_text = "L o a d i n g";
      int loading_text_width = u8g2.getStrWidth(loading_text.c_str());
      int loading_text_x_pos = 64 - (loading_text_width/2);
      u8g2.setCursor(loading_text_x_pos, 36);
      u8g2.print(loading_text);
      if (blink) {
        u8g2.drawBox(loading_text_x_pos + loading_text_width + 2, 34, 2, 2);
        blink = false;
      }
      else if (!blink) {
        blink = true;
      }
      yield();
    } while(u8g2.nextPage());
    delay(100);
  }

  /// monitor buttons, but only if we're powered on and the raspberry pi is ready.
  if (state_pwr == 1 && state_rpi == 1) {
    analogWrite(led_pwr, led_pwr_value);

    if (!splash_played) {
      splash_played = true;
      u8g2.clearBuffer();
      u8g2.sendBuffer();
      do {
        u8g2.setDrawColor(2);
        u8g2.setBitmapMode(1);
        u8g2.drawXBM(0, 22, u8g_minidsp_logo_width, u8g_minidsp_logo_height, u8g_minidsp_logo_bits);
        yield();
      } while(u8g2.nextPage());
      // disply the minisp logo for 1 second
      delay(1000);
    }

    /// monitor rotary
    uint8_t rot_res = rot.read();
    if (rot_res) {
      // increase volume step based on rotary speed
      int rotary_speed = rot.speed();
      if (rotary_speed <= 2) { // minimum volume step; when turning the rotary slowly
        volume_step = 0.5;
      }
      else if (rotary_speed > 16) { // maximum volume step; when turning the rotary fast
        volume_step = 10;
      }
      else {
        volume_step = ceil(pow(rotary_speed, 2)/40); // we want some nonlinearity between min and max
      }
      if (rot_res == DIR_CW) {
        volume_up();
      }
      if (rot_res == DIR_CCW) {
        volume_down();
      }
    }
    
    /// monitor buttons
    if (btn_switch_input.onPressed()) {
      cycle_source();
    }
  
    if (btn_switch_output.onPressed()) {
      cycle_out(false, -1);
    }
  
    if (btn_dsp.onPressed()) {
      // reset the counter
      btn_dsp_elapsed = 0;
      // check if the button is actually pressed, since the RBD::Button onReleased event fires on startup, for some reason.
      btn_dsp_pressed = true;
    }

    if (btn_dsp.onReleased()) {
      if (btn_dsp_pressed && btn_dsp_elapsed <= 200) {
        // short press.
        toggle_dsp();
        btn_dsp_pressed = false;
      }
      else if (btn_dsp_pressed && btn_dsp_elapsed >= 1000) {
        // long press

        is_calibrating = true;
        
        // set input to usb
        do {
          cycle_source();
        } while (txt_src != "USB");

        // set output to speakers
        // will turn on icepower, subwoofer and turn off headphone amp
        cycle_out(true, 0);

        // send a command to the Raspberry Pi so that it starts/stops the VirtualHere service
        Serial.write("C\n");

        btn_dsp_pressed = false;
      }
    }

    if (btn_mute.onPressed()) {
      // reset the counter
      btn_mute_elapsed = 0;
      // check if the button is actually pressed, since the RBD::Button onReleased() event fires on startup, for some reason.
      btn_mute_pressed = true;
    }

    if (btn_mute.onReleased()) {
      if (btn_mute_pressed && btn_mute_elapsed <= 200) {
        // short press: toggle mute
        toggle_mute();
        btn_mute_pressed = false;
      }
      else if (btn_mute_pressed && btn_mute_elapsed >= 1000) {
        // long press (>= 1s): display raspberry pi hostname and ip
        display_ip();
        btn_mute_pressed = false;
      }
    }

    /// monitor ir
    if (timeSinceLastIR > cmd_interval) {
      // clear previous IR command 100 ms after last IR interaction
      previous_ir_command = 0;
    }

    if (IrReceiver.decode()) {
      timeSinceLastIR = 0;
      digitalWrite(led_pwr, LOW);
      delay(10);
      analogWrite(led_pwr, led_pwr_value);
      unsigned long received_command = IrReceiver.decodedIRData.decodedRawData;

      switch(received_command) {
        case rx_minidsp_vol_up:
        case rx_apple_vol_up: {
          rx_held_cycles = 0;
          volume_step = 0.5;
          previous_ir_command = received_command;
          volume_up();
          break;
        }
        case rx_minidsp_vol_down:
        case rx_apple_vol_down: {
          rx_held_cycles = 0;
          volume_step = 0.5;
          previous_ir_command = received_command;
          volume_down();
          break;
        }
        case rx_minidsp_source:
        case rx_apple_prev: {
          rx_held_cycles = 0;
          previous_ir_command = received_command;
          cycle_source();
          break;
        }
        case rx_minidsp_mute:
        case rx_apple_play: {
          rx_held_cycles = 0;
          rx_mute_elapsed = 0;
          previous_ir_command = received_command;
          //toggle_mute();
          break;
        }
        case rx_minidsp_dsp:
        case rx_apple_menu: {
          rx_held_cycles = 0;
          previous_ir_command = received_command;
          toggle_dsp();
          break;
        }
        case rx_minidsp_prof_1: {
          // switch to speakers + harman curve
          rx_held_cycles = 0;
          previous_ir_command = received_command;
          cycle_out(true, 0);
          break;
        }
        case rx_minidsp_prof_2: {
          // switch to headphones
          rx_held_cycles = 0;
          previous_ir_command = received_command;
          cycle_out(true, 1);
          break;
        }
        case rx_minidsp_prof_3: {
          // switch to speakers + nad curve
          rx_held_cycles = 0;
          previous_ir_command = received_command;
          cycle_out(true, 2);
          break;
        }
        case rx_minidsp_prof_4: {
          // switch to speakers + dirac default curve
          rx_held_cycles = 0;
          previous_ir_command = received_command;
          cycle_out(true, 3);
          break;
        }
        case rx_apple_next: {
          // cycle outputs
          rx_held_cycles = 0;
          previous_ir_command = received_command;
          cycle_out(false, -1);
          break;
        }
        case rx_held: {
          if (rx_held_cycles <= 5) { // minimum volume step
            volume_step = 0.5;
          }
          else if (rx_held_cycles >= 40) { // maximum volume step
            volume_step = 10;
          }
          else {
            volume_step = ceil(pow(rx_held_cycles, 2)/144); // we want some nonlinearity betwen the min and max volume step
          }
          switch(previous_ir_command) {
            case rx_minidsp_vol_up:
            case rx_apple_vol_up: {
              volume_up();
              break;
            }
            case rx_minidsp_vol_down:
            case rx_apple_vol_down: {
              volume_down();
              break;
            }
            case rx_minidsp_mute:
            case rx_apple_play: {
              if (rx_mute_elapsed >= 1000 && rx_mute_elapsed <= 1100) {
                display_ip();
              }
              else if (rx_mute_elapsed > 0 && rx_mute_elapsed <= 200) {
                toggle_mute();
              }
              break;
            }
          }
          rx_held_cycles++;
          break;
        }
      }
      IrReceiver.resume();
    }

    if (oled_refresh) {
      // if the data has changed (volume, input, output, etc.) then display it

      state_oled = true;
      display_data();
    }
    else if (!oled_refresh && (oled_inactive_elapsed > inactive_interval) && state_oled == true && !is_calibrating) {
      // turn off the display if:
      // - there's no new data to display
      // - and enough time has passed
      // - and if the display is turned on

      u8g2.setPowerSave(1);
      state_oled = false;
    }
  }
  else
  {
    // turn everything on if the mute (minidsp) or play (apple remote) button is pressed
    if (IrReceiver.decode()) {
      //unsigned long received_command = ir_res.value;
      unsigned long received_command = IrReceiver.decodedIRData.decodedRawData;

      switch (received_command) {
        case rx_minidsp_mute:
        case rx_apple_play: {
          power_up();
          break;
        }
      }
      IrReceiver.resume();
    }
  }
}

/// methods

void display_data() {
  if (is_calibrating) {
    u8g2.setPowerSave(0); // wake up display
    u8g2.firstPage();
    do {
      u8g2.setFont(u8g2_font_micro_tr);
      String loading_text = "C a l i b r a t i n g";
      int loading_text_width = u8g2.getStrWidth(loading_text.c_str());
      int loading_text_x_pos = 64 - (loading_text_width/2);
      u8g2.setCursor(loading_text_x_pos, 36);
      u8g2.print(loading_text);

      String notice = "Disconnect VirtualHere to stop.";
      int notice_width = u8g2.getStrWidth(notice.c_str());
      int notice_x_pos = 64 - (notice_width/2);
      u8g2.setCursor(notice_x_pos, 44);
      u8g2.print(notice);
      yield();
    } while(u8g2.nextPage());
  }
  else {
    u8g2.firstPage();
    u8g2.setPowerSave(0);
    u8g2.setContrast(oled_brightness);
    u8g2.setFont(u8g2_font_7x14B_mf); // set font for input, output and static dsp text
    do {
      // input
      u8g2.setCursor(0,0);
      u8g2.setFontPosTop();
      u8g2.print(txt_src);
      u8g2.setFontPosBaseline();
      u8g2.drawHLine(0, 13, 128);
      // output
      int out_x_pos = 124 - (u8g2.getStrWidth(txt_out.c_str()));
      u8g2.setCursor(out_x_pos,0);
      u8g2.setFontPosTop();
      u8g2.print(txt_out);
      // dsp
      u8g2.setFontPosBottom();
      int dsp_static_x_pos = 64 - (u8g2.getStrWidth(txt_dsp_static.c_str()));
      u8g2.setCursor(dsp_static_x_pos, 64);
      u8g2.print(txt_dsp_static);
      u8g2.setFont(u8g2_font_7x14_mf); // set font for dynamic dsp text
      u8g2.setCursor(68, 64);
      u8g2.print(txt_dsp);
      // volume
      if (state_mute == 0) {
        u8g2.setFont(u8g2_font_profont29_mn); // set font for volume
        int vol_width = u8g2.getStrWidth(txt_vol.c_str()) + 13; // account for dB width & spacing 
        int vol_x_pos = 64 - (vol_width/2);
        u8g2.setCursor(vol_x_pos,32);
        u8g2.setFontPosCenter();
        u8g2.print(txt_vol);
        u8g2.setFontPosBaseline();
        u8g2.setCursor(54 + (vol_width/2), 39);
        u8g2.setFont(u8g2_font_t0_11_mf); // set font for dB text
        u8g2.print("dB");
      }
      else if (state_mute == 1) {
        // draw mute symbol instead of volume
        u8g2.setBitmapMode(1);
        u8g2.drawXBM(64-(u8g_mute_width/2),19,u8g_mute_width, u8g_mute_height, u8g_mute_bits);
      }
      // draw volume line if muted or not
      u8g2.drawHLine(0, 46, 128 + volume);
      u8g2.drawHLine(0, 47, 128 + volume);
      
      u8g2.setFont(u8g2_font_micro_tr);
      u8g2.setFontPosBaseline();

      if (state_icepower == 1) {
        // amp
        u8g2.setCursor(0, 62);
        u8g2.print("A");
      }
      if (state_iec2 == 1) {
        // subwoofer
        u8g2.setCursor(5, 62);
        u8g2.print("S");
      }
      if (state_iec3 == 1) {
        // headphones
        u8g2.setCursor(10, 62);
        u8g2.print("H");
      }
      if (state_5v == 1) {
        u8g2.setCursor(106, 62);
        u8g2.print("5V");
      }
      if (state_12v == 1) {
        u8g2.setCursor(116, 62);
        u8g2.print("12V");
      }

      yield();
    } while(u8g2.nextPage());
  }
  oled_refresh = false;
  oled_inactive_elapsed = 0;
}

void display_ip() {
  // refresh IP & hostname info, whenever the method is called;
  // the connection between the Raspberry Pi and the Arduino is established before
  // the networking is up and an IP is obtained from the DHCP server
  Serial.write("IP\n");
  Serial.write("H\n");

  u8g2.setPowerSave(0);
  u8g2.firstPage();
  do {
    u8g2.setFont(u8g2_font_micro_tr);
    u8g2.setFontPosTop();
    u8g2.setCursor(0,0);
    u8g2.print("I n f o :");
    u8g2.setFontPosBaseline();
    u8g2.drawHLine(0, 10, 128);
    u8g2.setCursor(0, 19);
    u8g2.print("hostname:");
    u8g2.setCursor(0, 28);
    u8g2.print(local_hostname);
    u8g2.setCursor(0, 38);
    u8g2.print("ip:");
    u8g2.setCursor(0, 47);
    u8g2.print(local_ip);
    yield();
  } while(u8g2.nextPage());
  delay(5000);
  oled_refresh = true;
}

void cycle_out(bool direct_set, int out_value) {
  output_state o = output_states[0]; // placeholder

  if (direct_set && out_value != -1) {
    o = output_states[out_value];
  }
  else {
    o = output_states[output_states[state_out].next_out];
  }

  digitalWrite(out_ctrl_11, o.out_11);
  digitalWrite(out_ctrl_12, o.out_12);
  digitalWrite(out_ctrl_21, o.out_21);
  digitalWrite(out_ctrl_22, o.out_22);

  if (state_dsp != o.dsp_state) {
    toggle_dsp();
  }
  Serial.println(o.pi_command);
  txt_out = o.display_text;

  state_out = o.current_out;

  // should convert the strings to bytes and use Serial.write
  // this lazy solution allows concatenation and automatically adds a line break to the end.
  Serial1.println("IEC1-" + String(o.iec1));
  Serial1.println("IEC2-" + String(o.iec2));
  Serial1.println("IEC3-" + String(o.iec3));
  
  // for some reason this is not working:
  //Serial2.println(String(o.amplifier1));
  // but this is:
  if (o.amplifier1 == 0) {
    Serial2.write("0\n");
  }
  else if (o.amplifier1 == 1) {
    Serial2.write("1\n");
  }
  
  oled_refresh = true;
}

void cycle_source() {
  source_state i = source_states[source_states[state_src].next_src];
  digitalWrite(in_ctrl_digi_a, i.in_digi_a);
  digitalWrite(in_ctrl_digi_b, i.in_digi_b);
  digitalWrite(in_ctrl_analog_1, i.in_analog_1);
  digitalWrite(in_ctrl_analog_2, i.in_analog_2);
  digitalWrite(in_ctrl_analog_3, i.in_analog_3);
  digitalWrite(in_ctrl_analog_4, i.in_analog_4);
  Serial.println(i.pi_command);
  txt_src = i.display_text;

  state_src = i.current_src;
  if (i.target_volume <= 0) { // volume can't be a positive number so we'll use 0 as a test if we need to change the volume or not
    // subtract the target volume from the curent volume
    // if the difference is positive then lower volume by the difference
    // if the difference is negative then raise the volume by the difference
    // store and then restore the volume_step
    float old_volume_step = volume_step;
    float volume_difference = volume - i.target_volume;
    volume_step = abs(volume_difference);
    if (volume_difference > 0) {
      volume_down();
    }
    else if (volume_difference < 0) {
      volume_up();
    }
    volume_step = old_volume_step;
  }
  oled_refresh = true;
}

void toggle_mute() {
  mute_state m = mute_states[mute_states[state_mute].next_mute];
  Serial.println(m.pi_command);
  txt_mute = m.display_text;
  state_mute = m.current_mute;
  oled_refresh = true;
}

void toggle_dsp() {
  dsp_state d = dsp_states[dsp_states[state_dsp].next_dsp];
  Serial.println(d.pi_command);
  txt_dsp = d.display_text;
  state_dsp = d.current_dsp;
  oled_refresh = true;
}

void toggle_amplifiers() {
  // icepower
  switch (state_icepower) {
    case 0: {
      Serial2.write("1\n");
      break;
    }
    case 1: {
      Serial2.write("0\n");
      break;
    }
  }
  
  // amp2 on Serial3/unused
  switch (state_amp2) {
    case 0: {
      Serial3.write("1\n");
      break;
    }
    case 1: {
      Serial3.write("0\n");
      break;
    }
  }
}

void power_up() {
  power_state p = power_states[power_states[state_pwr].next_pwr];
  state_pwr = p.current_pwr;

  Serial3.write("1\n"); // PSU

  // turn on power amplifiers after 4 seconds via Serial1, Serial2 and Serial3
  delay(4000);
  
  Serial1.write("IEC2-1\n"); // subwoofer
  Serial2.write("1\n"); // icepower
  
  oled_refresh = true;
}

void power_down() {
  power_state p = power_states[power_states[state_pwr].next_pwr];
  Serial.println(p.pi_command);

  // wait 10 seconds, read rpi_tx. if low then cut power to the raspberry pi and minidsp
  // this is done so the raspberry pi has time to shut down. when the pi is off its uart tx pin gets pulled low.
  u8g2.setPowerSave(0);
  u8g2.firstPage();
  do {
    u8g2.setFontMode(1);
    u8g2.setDrawColor(2);
    u8g2.drawXBM(0, 22, u8g_minidsp_logo_width, u8g_minidsp_logo_height, u8g_minidsp_logo_bits);
    yield();
  } while(u8g2.nextPage());
  delay(3000); // 3 second pause

  for (int i = 0; i<=70; i++) { // roughly 7 second pause (70 loops * 100ms, give or take)
    u8g2.firstPage();
    do {
      u8g2.setFont(u8g2_font_micro_tr);
      String shutdown_text = "G o o d b y e";
      int shutdown_text_width = u8g2.getStrWidth(shutdown_text.c_str());
      int shutdown_text_x_pos = 64 - (shutdown_text_width/2);
      u8g2.setCursor(shutdown_text_x_pos, 36);
      u8g2.print(shutdown_text);
      if (blink) {
        u8g2.drawBox(shutdown_text_x_pos + shutdown_text_width + 2, 34, 2, 2);
        blink = false;
      }
      else if (!blink) {
        blink = true;
      }
      yield();
    } while(u8g2.nextPage());
    delay(100);
  }
  
  if (digitalRead(rpi_tx) == 0) {
    // turn off power amplifiers first
    Serial2.write("0\n"); // icepower
    
    // wait 2 seconds, then turn off the minidsp and raspberry pi.
    delay(2000);
    
    Serial1.write("0\n");
    
    state_pwr = p.current_pwr;
    state_rpi = 0;
    splash_played = false;
    state_oled = 0;
    
    digitalWrite(led_pwr, LOW);

    // power down RPi and MiniDSP
    Serial3.write("0\n"); // PSU
  }

  u8g2.clearBuffer();
  u8g2.sendBuffer();
  u8g2.setPowerSave(1);
}

void volume_up() {
  // send volume up command only if 500 ms have elapsed since last command
  if (cmd_elapsed > cmd_interval) {
    if (state_mute == 1) {
      toggle_mute();
    }
    Serial.println("V+" + String(volume_step));
    oled_refresh = true;
    cmd_elapsed = 0;
  }
}

void volume_down() {
  // send volume down command only if 500 ms have elapsed since last command
  if (cmd_elapsed > cmd_interval) {
    if (state_mute == 1) {
      toggle_mute();
    }
    Serial.println("V-" + String(volume_step));
    oled_refresh = true;
    cmd_elapsed = 0;
  }
}

# pragma region helper methods
// check if string starts with prefix
int startsWith(const char *pre, const char *str) {
  return strncmp(pre, str, strlen(pre)) == 0;
}
# pragma endregion

# pragma region serial event handlers
void serialEventRun(void) {
  if (Serial.available()) {
    char inStrSer0[100];
    int idxInStrSer0 = 0;
    delay(150);
    serial0Event(inStrSer0, idxInStrSer0);
  }
  if (Serial1.available()) {
    char inStrSer1[100];
    int idxInStrSer1 = 0;
    delay(50);
    serial1Event(inStrSer1, idxInStrSer1);
  }
  if (Serial2.available()) {
    char inStrSer2[100];
    int idxInStrSer2 = 0;
    delay(50);
    serial2Event(inStrSer2, idxInStrSer2);
  }
  if (Serial3.available()) {
    char inStrSer3[100];
    int idxInStrSer3 = 0;
    //delay(50);
    serial3Event(inStrSer3, idxInStrSer3);
  }
}
# pragma endregion

# pragma region Serial
// Raspberry Pi
void serial0Event(char inStr[], int idxInStr) {
  bool inStrComplete = false;
  char delimiter = '\n';

  while (Serial.available()) {
    inStr[idxInStr] = Serial.read();
    if (inStr[idxInStr] == delimiter) {
      inStrComplete = true;
      break;
    }
    if (!inStrComplete) {
      idxInStr++;
    }
    else { break; }
  }
  inStr[idxInStr] = '\0';

  if (inStrComplete) {
    inStrComplete = false;
    serial0EventTokenize(inStr);
  }
}

void serial0EventTokenize(char inStr[]) {
  char *cmd = strtok(inStr, "\n");

  while (cmd) {
    serial0EventExecute(cmd);
    cmd = strtok(NULL, "\n");
  }
}

void serial0EventExecute(char cmd[]) {
  // R; RPi ready.
  if (!strcmp(cmd, "R")) {
    state_rpi = 1;
    is_calibrating = false;
    // get ip and hostname
    analogWrite(led_pwr, led_pwr_value);
  }
  // input:
  //  INtoslink
  //  INanalog
  //  INusb
  if (startsWith("IN", cmd) == 1) {
    char activeMiniDSPInput = cmd[2];
    switch (activeMiniDSPInput) {
      case 't': { // toslink
        state_src = 0;
        break;
      }
      case 'a': { // analog
        state_src = 4;
        break;
      }
      case 'u': { // usb
        state_src = 8;
        break;
      }
    }

    source_state i = source_states[state_src];
    txt_src = i.display_text;
  }
  // profile:
  //  P1
  //  P2
  //  P3
  //  P4
  if (startsWith("P", cmd) == 1) {
    int activeMiniDSPProfile = cmd[1] - '0';
    state_out = activeMiniDSPProfile -1;
    output_state o = output_states[state_out];
    txt_out = o.display_text;
    cycle_out(true, state_out);
  }
  // dirac:
  //  DTrue
  if (startsWith("D", cmd) == 1) {
    char miniDSPDiracState = cmd[1];
    switch (miniDSPDiracState) {
      case 'T': {
        state_dsp = 1;
        break;
      }
      case 'F': 
      default: {
        state_dsp = 0;
        break;
      }
    }

    dsp_state d = dsp_states[state_dsp];
    txt_dsp = d.display_text;
  }
  // mute:
  //  MTrue
  if (startsWith("M", cmd) == 1) {
    char miniDSPMuteState = cmd[1];
    switch (miniDSPMuteState) {
      case 'T': {
        state_mute = 1;
        break;
      }
      case 'F':
      default: {
        state_mute = 0;
        break;
      }
    }

    mute_state m = mute_states[state_mute];
    txt_mute = m.display_text;
  }
  // volume:
  //  V-128.50 to V0
  if (startsWith("V", cmd) == 1) {
    memmove(cmd, cmd+1, strlen(cmd));
    float miniDSPVolume = atof(cmd);
    volume = miniDSPVolume;
    txt_vol = String(volume);
  }
  // ip:
  //  IP128.128.128.128
  if (startsWith("IP", cmd) == 1) {
    memmove(cmd, cmd+2, strlen(cmd));
    local_ip = cmd;
  }
  // hostname:
  //  Habcdefghijklmnopq
  if (startsWith("H", cmd) == 1) {
    memmove(cmd, cmd+1, strlen(cmd));
    local_hostname = cmd;
  }
  // debug
  // send variable values to RPi
  if (!strcmp(cmd, "BUG")) {
    Serial.write("Debug called\n");
    char *state;
    state = gatherVariables();
    debugPrint(state);
    free(state);
  }
  oled_refresh = true;
}

# pragma endregion

# pragma region Serial1
// IEC ports (1-3)
void serial1Event(char inStr[], int idxInStr) {
  bool inStrComplete = false;

  while (Serial1.available()) {
    inStr[idxInStr] = Serial1.read();
    if (inStr[idxInStr] == delimiter) {
      inStrComplete = true;
    }
    idxInStr++;
  }
  inStr[idxInStr] = '\0';

  if (inStrComplete) {
    inStrComplete = false;

#ifdef DEBUG
  debugPrint(inStr);
#endif

    serial1EventTokenize(inStr);
  }
}

void serial1EventTokenize(char inStr[]) {
  char *cmd = strtok(inStr, "\n");

  while (cmd) {
    serial1EventExecute(cmd);
    cmd = strtok(NULL, "\n");
  }
}

void serial1EventExecute(char cmd[]) {
  // PSU-IECx-y
  int iecNo = cmd[7] - '0';
  int iecState = cmd[9] - '0';

  switch (iecNo) {
    case 1: {
      state_iec1 = iecState;
      break;
    }
    case 2: {
      state_iec2 = iecState;
      break;
    }
    case 3: {
      state_iec3 = iecState;
      break;
    }
  }

  oled_refresh = true;
}
# pragma endregion

# pragma region Serial2
// ICEPower amplifier
void serial2Event(char inStr[], int idxInStr) {
  bool inStrComplete = false;

  while (Serial2.available()) {
    inStr[idxInStr] = Serial2.read();
    if (inStr[idxInStr] == delimiter) {
      inStrComplete = true;
    }
    idxInStr++;
  }
  inStr[idxInStr] = '\0';

  if (inStrComplete) {
    inStrComplete = false;

#ifdef DEBUG
  debugPrint(inStr);
#endif

    serial2EventTokenize(inStr);
  }
}

void serial2EventTokenize(char inStr[]) {
  char *cmd = strtok(inStr, "\n");

  while (cmd) {
    serial2EventExecute(cmd);
    cmd = strtok(NULL, "\n");
  }
}

void serial2EventExecute(char cmd[]) {
  // ICEx
  int iceState = cmd[3] - '0';

  state_icepower = iceState;

  oled_refresh = true;
}
# pragma endregion

# pragma region Serial3
// 5V, 9V, 12V PSU
void serial3Event(char inStr[], int idxInStr) {
  bool inStrComplete = false;

  while (Serial3.available()) {
    inStr[idxInStr] = Serial3.read();
    if (inStr[idxInStr] == delimiter) {
      inStrComplete = true;
    }
    idxInStr++;
  }
  inStr[idxInStr] = '\0';

  if (inStrComplete) {
    inStrComplete = false;

#ifdef DEBUG
  debugPrint(inStr);
#endif

    serial3EventTokenize(inStr);
  }
}

void serial3EventTokenize(char inStr[]) {
  char *cmd = strtok(inStr, "\n");

  while (cmd) {
    serial3EventExecute(cmd);
    cmd = strtok(NULL, "\n");
  }
}

void serial3EventExecute(char cmd[]) {
  // PSU-005V-x
  // PSU-012V-y
  memmove(cmd, cmd+5, strlen(cmd));

  int statePSU = cmd[3] - '0';
  
  // 05V-x
  if (startsWith("05", cmd) == 1) {
    state_5v = statePSU;
  }

  // 12V-y
  if (startsWith("12", cmd) == 1) {
    state_12v = statePSU;
  }

  oled_refresh = true;
}
# pragma endregion

# pragma region debug stuff
char * gatherVariables() {
  char *state = malloc(1000);
  char tmp[10];
  // state_pwr
  strcat(state, "state_pwr: ");
  sprintf(tmp, sizeof(tmp), "%d", state_pwr);
  strcat(state, tmp);
  strcat(state, "\n");
  // state_rpi
  strcat(state, "state_rpi: ");
  sprintf(tmp, sizeof(tmp), "%d", state_rpi);
  strcat(state, tmp);
  strcat(state, "\n");
  // state_mute
  strcat(state, "state_mute: ");
  sprintf(tmp, sizeof(tmp), "%d", state_mute);
  strcat(state, tmp);
  strcat(state, "\n");
  // state_dsp
  strcat(state, "state_dsp: ");
  sprintf(tmp, sizeof(tmp), "%d", state_dsp);
  strcat(state, tmp);
  strcat(state, "\n");
  // state_src
  strcat(state, "state_src: ");
  sprintf(tmp, sizeof(tmp), "%d", state_src);
  strcat(state, tmp);
  strcat(state, "\n");
  // state_out
  strcat(state, "state_out: ");
  sprintf(tmp, sizeof(tmp), "%d", state_out);
  strcat(state, tmp);
  strcat(state, "\n");
  // volume
  strcat(state, "volume: ");
  sprintf(tmp, sizeof(tmp), "%.2f", volume);
  strcat(state, tmp);
  strcat(state, "\n");
  // is_calibrating
  strcat(state, "is_calibrating: ");
  sprintf(tmp, sizeof(tmp), "%d", is_calibrating);
  strcat(state, tmp);
  strcat(state, "\n");
  // state_5v
  strcat(state, "state_5v: ");
  sprintf(tmp, sizeof(tmp), "%d", state_5v);
  strcat(state, tmp);
  strcat(state, "\n");
  // state_12v
  strcat(state, "state_12v: ");
  sprintf(tmp, sizeof(tmp), "%d", state_12v);
  strcat(state, tmp);
  strcat(state, "\n");
  // state_iec1
  strcat(state, "state_iec1: ");
  sprintf(tmp, sizeof(tmp), "%d", state_iec1);
  strcat(state, tmp);
  strcat(state, "\n");
  // state_iec2
  strcat(state, "state_iec2: ");
  sprintf(tmp, sizeof(tmp), "%d", state_iec2);
  strcat(state, tmp);
  strcat(state, "\n");
  // state_iec3
  strcat(state, "state_iec3: ");
  sprintf(tmp, sizeof(tmp), "%d", state_iec3);
  strcat(state, tmp);
  strcat(state, "\n");
  // state_icepower
  strcat(state, "state_icepower: ");
  sprintf(tmp, sizeof(tmp), "%d", state_icepower);
  strcat(state, tmp);
  strcat(state, "\n");
  // state_amp2
  strcat(state, "state_amp2: ");
  sprintf(tmp, sizeof(tmp), "%d", state_amp2);
  strcat(state, tmp);
  strcat(state, "\n");
  // ldr_value
  strcat(state, "ldr_value: ");
  sprintf(tmp, sizeof(tmp), "%d", ldr_value);
  strcat(state, tmp);
  strcat(state, "\n");
  // led_pwr_value
  strcat(state, "led_pwr_value: ");
  sprintf(tmp, sizeof(tmp), "%d", led_pwr_value);
  strcat(state, tmp);
  strcat(state, "\n");

  return state;
}
// Send string to Serial1 for debugging
void debugPrint(char str[]) {
  Serial.write("BUG;");
  Serial.write(str);
  Serial.write("\n");
}
# pragma endregion
