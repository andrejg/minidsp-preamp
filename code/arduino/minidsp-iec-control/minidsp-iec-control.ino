#include <elapsedMillis.h>

// front panel
#define led1 11
#define led2 10
#define led3 9
#define led4 6

#define ldr A5
int ldrValue = 0;
int ledValue = 0;

// relay
#define rel1 A0 // relays 1, 2 - iec1
#define rel2 A1 // relays 3, 4 - iec2
#define rel3 A2 // relays 5, 6 - iec3

// state variables
// set default states here
int stateIEC1 = 1;
int stateIEC2 = 0;
int stateIEC3 = 0;

int statePower = 0;

elapsedMillis ledBlinkElapsed;

void setup() {
  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
  pinMode(led3, OUTPUT);
  pinMode(led4, OUTPUT);
  pinMode(ldr, INPUT);
  pinMode(rel1, OUTPUT);
  pinMode(rel2, OUTPUT);
  pinMode(rel3, OUTPUT);

  digitalWrite(rel1, stateIEC1);
  digitalWrite(rel2, stateIEC2);
  digitalWrite(rel3, stateIEC3);
  
  Serial.begin(9600);
}

void loop() {
  ldrValue = (analogRead(ldr)/4);
  ledValue = 255 - (ldrValue > 254 ? 254 : ldrValue);

  if (stateIEC1 == 1) {
    analogWrite(led1, ledValue);
  }
  else if (stateIEC1 == 0) {
    digitalWrite(led1, LOW);
  }

  if (stateIEC2 == 1) {
    analogWrite(led2, ledValue);
  }
  else if (stateIEC2 == 0) {
    digitalWrite(led2, LOW);
  }
  
  if (stateIEC3 == 1) {
    analogWrite(led3, ledValue);
  }
  else if (stateIEC3 == 0) {
    digitalWrite(led3, LOW);
  }
  
  if (stateIEC2 == 0 && stateIEC3 == 0) {
    if (ledBlinkElapsed >= 1500) {
      analogWrite(led4, ledValue);
      delay(25);
      digitalWrite(led4, 0);
      ledBlinkElapsed = 0;
    }
  }
  else {
    analogWrite(led4, ledValue);
  }
}

void powerOnIec(int iec) {
  switch (iec) {
    case 1: {
      digitalWrite(rel1, HIGH);
      stateIEC1 = 1;
      Serial.write("PSU-IEC1-1\n");
      break;
    }
    case 2: {
      digitalWrite(rel2, HIGH);
      stateIEC2 = 1;
      Serial.write("PSU-IEC2-1\n");
      break;
    }
    case 3: {
      digitalWrite(rel3, HIGH);
      stateIEC3 = 1;
      Serial.write("PSU-IEC3-1\n");
      break;
    }
  }
}

void powerOffIec(int iec) {
  switch (iec) {
    case 1: {
      digitalWrite(rel1, LOW);
      stateIEC1 = 0;
      Serial.write("PSU-IEC1-0\n");
      break;
    }
    case 2: {
      digitalWrite(rel2, LOW);
      stateIEC2 = 0;
      Serial.write("PSU-IEC2-0\n");
      break;
    }
    case 3: {
      digitalWrite(rel3, LOW);
      stateIEC3 = 0;
      Serial.write("PSU-IEC3-0\n");
      break;
    }
  }
}

# pragma region serial event handling
void serialEventRun(void) {
  if (Serial.available()) {
    char inStr[100];
    int idxInStr = 0;
    delay(50);
    serialEvent(inStr, idxInStr);
  }
}

void serialEvent(char inStr[], int idxInStr) {
  bool inStrComplete = false;
  char delimiter = '\n';

  while (Serial.available()) {
    inStr[idxInStr] = Serial.read();
    if (inStr[idxInStr] == delimiter) {
      inStrComplete = true;
    }
    idxInStr++;
  }
  inStr[idxInStr] = '\0';

  if (inStrComplete) {
    inStrComplete = false;
    serialEventTokenize(inStr);
  }
}

void serialEventTokenize(char inStr[]) {
  char *cmd = strtok(inStr, "\n");

  while (cmd) {
    serialEventExecute(cmd);
    cmd = strtok(NULL, "\n");
  }
}

void serialEventExecute(char cmd[]) {
  // 0
  if (!strcmp(cmd, "0")) {
    // power down everything except IEC1
    for (int i = 2; i <= 3; i++) {
      powerOffIec(i);
    }
    //buzzOff();
  }
  // 1
  if (!strcmp(cmd, "1")) {
    // power up everything
    for (int i = 1; i <= 3; i++) {
      powerOnIec(i);
    }
    //buzzOn();
  }
  // IECx-0
  // IECx-1
  if (startsWith("IEC", cmd) == 1) {
    int iecNo = cmd[3] - '0';
    int iecCmd = cmd[5] - '0';

    switch (iecCmd) {
      case 0: {
        powerOffIec(iecNo);
        break;
      }
      case 1: {
        powerOnIec(iecNo);
        break;
      }
    }
  }
  // BLINK
  // sync blink w/ MCU on preamp & amp
  if (!strcmp(cmd, "BLINK")) {
    ledBlinkElapsed = 1500;
  }
}
# pragma endregion

# pragma region helper methods
int startsWith(const char *pre, const char *str)
{
    return strncmp(pre, str, strlen(pre)) == 0;
}
# pragma endregion
